**[UNMAINTAINED]**
==================
**Maintenance for this tool continues in [THIS REPO](https://bitbucket.org/DAlgma/habitica-tools/). With issues or suggestions please refer to the latter!**

# Habitica Party Tools

Some Tools to help Habitica parties decide which Quest they should do next.

### Prerequisites

NodeJs / Yarn Package Manager


### Installing


> yarn install
> yarn start


## Deployment

I use the awesome [surge.sh](http://surge.sh)

## Built With

* [Preact](https://preactjs.com/) - The web framework used
* [Mobx](https://mobx.js.org/) - State Management
* [Semantic UI](https://semantic-ui.com) - Styles

## Contributing

Contact me if you want to contribute code or stuff


## Authors


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Habitica for making my life more organized 
